import java.util.Scanner; //import an instance of the class
public class Yahtzee {
  public static void main(String[] args){
    Scanner myScanner; // declaration
    myScanner = new Scanner (System.in); //construction statement
    System.out.println("If you want to roll randomly, input '1'; input 2 if you want to type in a 5 digit number representing the result of a specific roll");
    // indication of the choices user can make
    int decision = myScanner.nextInt(); //declare integer variable choice and input choice
    int firstDigit = 0; //declare first roll
    int secondDigit = 0; //declare second roll
    int thirdDigit = 0; //declare third roll
    int fourthDigit = 0; //declare fourth roll
    int fifthDigit = 0; //declare fifth roll
   
    if (decision == 1) { ///////////// Method 1
      firstDigit = (int) (Math.random()*6)+1; //randomly pick a integer from 1 to 6
      secondDigit = (int) (Math.random()*6)+1; //randomly pick a integer from 1 to 6
      thirdDigit = (int) (Math.random()*6)+1; //randomly pick a integer from 1 to 6
      fourthDigit = (int) (Math.random()*6)+1; //randomly pick a integer from 1 to 6
      fifthDigit = (int) (Math.random()*6)+1; //randomly pick a integer from 1 to 6
      System.out.println("the number of five rolls are: " + firstDigit +" "+ secondDigit +" "+ thirdDigit +" "+ fourthDigit +" "+ fifthDigit);
    } 
    
    
    else if (decision == 2){ ///////////// Method 2
    System.out.println("Type in a 5 digit number representing the result of a specific roll (one digit at a time): ");
         firstDigit = myScanner.nextInt(); // input for firstDigit
      if (firstDigit >= 1&&firstDigit <= 6){
         secondDigit = myScanner.nextInt();} // input the second digit if the first digit is between 1-6
      else { System.out.println("you enter a wrong number");
             System.exit(0);} //shows error
      
               
      if (secondDigit >=1 &&secondDigit <= 6) {
         thirdDigit = myScanner.nextInt();}  //input the third digit if the second digit is between 1-6
      else { System.out.println("you enter a wrong number");
             System.exit(0);}  //shows error
          
      if (thirdDigit >= 1&&thirdDigit <= 6) {
         fourthDigit = myScanner.nextInt();}  //input the fourth digit if the third digit is between 1-6
      else { System.out.println("you enter a wrong number");
             System.exit(0);}//shows error
           
      if (fourthDigit >= 1&&fourthDigit <= 6) {
         fifthDigit = myScanner.nextInt();}  //input the fifth digit if the fourth digit is between 1-6
      else { System.out.println("you enter a wrong number");
             System.exit(0);} //shows error
                
      if (fifthDigit >= 1&&fifthDigit <= 6) {  
        System.out.println(firstDigit +" "+ secondDigit +" "+ thirdDigit +" "+ fourthDigit +" "+ fifthDigit);}
      // the output of the numbers we typed in will be shown after all the conditions are satisfied
      else { System.out.println("you enter a wrong number");
             System.exit(0);}//shows error
    }
      /////////////// Upper Section  
      int Aces = 0; //declaration of sum of the Aces
      if (firstDigit==1) {
        Aces = Aces + 1;
      }  //adding up the Aces value
      if (secondDigit==1) {
        Aces = Aces + 1;
      } //adding up the Aces value
      if (thirdDigit==1) {
        Aces = Aces + 1;
      } //adding up the Aces value
      if (fourthDigit==1) {
        Aces = Aces + 1;
      } //adding up the Aces value
      if (fifthDigit==1) {
        Aces = Aces + 1;
      } //adding up the Aces value
    
      System.out.println("The sum of Aces is " + Aces); 
       
      int Twos = 0; //declaration of sum of the twos
      if (firstDigit==2) {
        Twos = Twos + 2;
      } //adding up the twos value
      if (secondDigit==2) {
        Twos = Twos + 2;
      } //adding up the twos value
      if (thirdDigit==2) {
        Twos = Twos + 2;
      } //adding up the twos value
      if (fourthDigit==2) {
        Twos = Twos + 2;
      } //adding up the twos value
      if (fifthDigit==2) {
        Twos = Twos + 2;
      } //adding up the twos value
      System.out.println("The sum of Twos is " + Twos);
    
    
      int Threes = 0; //declaration of sum of the threes
      if (firstDigit==3) {
        Threes = Threes + 3;
      } //adding up the threes value
      if (secondDigit==3) {
        Threes = Threes + 3;
      } //adding up the threes value
      if (thirdDigit==3) {
        Threes = Threes + 3;
      } //adding up the threes value
      if (fourthDigit==3) {
        Threes = Threes + 3;
      } //adding up the threes value
      if (fifthDigit==3) {
        Threes =Threes + 3;
      } //adding up the threes value
    
      System.out.println("The sum of Threes is " + Threes);
    
    
      int Fours = 0;  //declaration of sum of the fours
      if (firstDigit==4) {
        Fours = Fours + 4;
      } //adding up the fours value
      if (secondDigit==4) {
        Fours = Fours + 4;} //adding up the fours value
      if (thirdDigit==4) {Fours = Fours + 4;
                         } //adding up the fours value
      if (fourthDigit==4) {
        Fours = Fours + 4;
      } //adding up the fours value
      if (fifthDigit==4) {
        Fours = Fours + 4;
                         } //adding up the fours value
      System.out.println("The sum of Fours is " + Fours);
    
    
      int Fives = 0;  //declaration of sum of the fives
      if (firstDigit==5) {
        Fives = Fives + 5;
      } //adding up the fives value
      if (secondDigit==5) {
        Fives = Fives + 5;
      } //adding up the fives value
      if (thirdDigit==5) {
        Fives = Fives + 5;
      } //adding up the fives value
      if (fourthDigit==5) {
        Fives = Fives + 5;
      } //adding up the fives value
      if (fifthDigit==5) {
        Fives = Fives + 5;
      } //adding up the fives value
      System.out.println("The sum of Fives is " + Fives);
    
    
      int Sixes = 0;  //declaration of sum of the sixes
      if (firstDigit==6) {
        Sixes = Sixes + 6;
      } //adding up the sixes value
      if (secondDigit==6) {
        Sixes = Sixes + 6;} //adding up the sixes value
      if (thirdDigit==6) {Sixes = Sixes + 6;
                         } //adding up the sixes value 
      if (fourthDigit==6) {
        Sixes = Sixes + 6;
      } //adding up the sixes value
      if (fifthDigit==6) {
        Sixes = Sixes + 6;
      } //adding up the sixes value
      System.out.println("The sum of Sixes is " + Sixes);
   
      int upperInitialTotal = Aces + Twos + Threes + Fours + Fives +Sixes; //The sum of the aces, twos, threes, fours, fives, and sixes components is the upper initial total
      
      if (upperInitialTotal>63){//if upper initial total is 63 or over, we add a bonus score of 35 to it
        upperInitialTotal = upperInitialTotal + 35;
      } 
      System.out.println("The upper initial total is " + upperInitialTotal); //output of the upper initial total
      System.out.println("The upper initial total (including bonus) is " + upperInitialTotal);//output of the upper initial total plus bonus
     
      ////////// Lower Section
      
      int numOfAces=Aces; //number of the Aces
      int numOfTwos=Twos/2; //number of the twos
      int numOfThrees=Threes/3; //number of the threes
      int numOfFours=Fours/4; //number of the fours
      int numOfFives=Fives/5; //number of the fives
      int numOfSixes=Sixes/6; //number of the sixes
      
      int threeOfAKind=0; //declaration of the score of three of a kind
      int fourOfAKind=0; //declaration of the score of four of a kind
      int yahtzee=0; // declaration of the score of yahtzee
      int fullHouse=0; //declaration of the score of full house
      int smallStraight=0; //declaration of the score of small straight
      int largeStraight=0; //declaration of the score of large straight
      int chance=0; //declaration of the score of chance
      
      ////////////when we have three of a kind
      if (numOfAces==3){
        threeOfAKind=3;
      } // 3 Aces add up to 3
      if (numOfTwos==3){
        threeOfAKind=6;
      } // 3 twos add up to 6
      if (numOfThrees==3){
        threeOfAKind=9;
      } // 3 threes add up to 9
      if (numOfFours==3){
        threeOfAKind=12;
      } // 3 fours add up to 12
      if (numOfFives==3){
        threeOfAKind=15;
      } // 3 fives add up to 15
      if (numOfSixes==3){
        threeOfAKind=18;
      } // 3 sixes add up to 18
      System.out.println("The three of a kind: " + threeOfAKind);
      
      ////////////when we have four of a kind
      if (numOfAces==4){
        fourOfAKind=4;
      } //4 Aces add up to 4
      if (numOfTwos==4){
        fourOfAKind=8;
      } //4 twos add up to 8
      if (numOfThrees==4){
        fourOfAKind=12;
      } //4 threes add up to 12
      if (numOfFours==4){
        fourOfAKind=16;
      } //4 fours add up to 16
      if (numOfFives==4){
        fourOfAKind=20;
      } //4 fives add up to 20
      if (numOfSixes==4){
        fourOfAKind=24;
      } //4 sixes add up to 24
      System.out.println("The four of a kind: " + fourOfAKind);
      
      // The Yahtzee component is nonzero if all five dice are identical, in which case its value is 50
      if (numOfAces==5||numOfTwos==5||numOfThrees==5||numOfFours==5||numOfFives==5||numOfSixes==5)
      {yahtzee=50;} 
      System.out.println("The Yahtzee: " + yahtzee);
      
      //The full house component is nonzero if a pair of dice are identical and the other three are different but identical to themselves,
      //in which case its value is 25.
      if ((numOfAces==3 || numOfTwos==3 || numOfThrees==3 || numOfFours==3 || numOfFives==3 || numOfSixes==3) 
          && (numOfAces==2 || numOfTwos==2 || numOfThrees==2 || numOfFours==2 || numOfFives==2 || numOfSixes==2))
      {fullHouse=25;}
      System.out.println("The full house: " + fullHouse);
      
     //The small straight component is nonzero if a four dice exhibit sequential values
     if ((numOfAces==1&&numOfTwos==1&&numOfThrees==1&&numOfFours==1)
         ||(numOfTwos==1&&numOfThrees==1&&numOfFours==1&&numOfFives==1)
         ||(numOfThrees==1&&numOfFours==1&&numOfFives==1&&numOfSixes==1))
     {smallStraight=30;}
     System.out.println("The small straight: " + smallStraight);
     
     //The large straight component is nonzero if all five dice exhibit sequential values
     if ((numOfAces==1&&numOfTwos==1&&numOfThrees==1&&numOfFours==1&&numOfFives==1)
         ||(numOfTwos==1&&numOfThrees==1&&numOfFours==1&&numOfFives==1&&numOfSixes==1))
     {largeStraight=40;}
     System.out.println("The large straight: " + largeStraight);
    
    //Chance component is equal to the sum of the value on all six dice.
     chance=firstDigit+secondDigit+thirdDigit+fourthDigit+fifthDigit; 
     System.out.println("The chance: " + chance);
     
     int lowerTotal=threeOfAKind+fourOfAKind+yahtzee+fullHouse+smallStraight+largeStraight+chance; //lower section total is the sum of the above components.
     int grandTotal=lowerTotal+upperInitialTotal; //The grand total is equal to the sum of upper initial total (including bonus) and lower section total
     System.out.println("The lower section total: " + lowerTotal); // output of the sum of the lower section 
     System.out.println("The grand total: " + grandTotal); // output of the sum of grand total
  
  } // end of the main method
} // end of the class