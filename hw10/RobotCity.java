//Shenyi Yu
//CSE002
//April 25 2018
//This program is designed to get a 2D dimensional arrays to represent a city
import java.util.Random;
public class RobotCity{
  public static void main(String[] args){
    Random random = new Random();
    int dimension1 = random.nextInt(6)+10;
    int dimension2 = random.nextInt(6) + 10;
    int[][] ct = buildCity(dimension1, dimension2, true);
    //cityArray(ct, true);
    System.out.println("The city is displaying...");
    display(ct, true);
    invade(ct,true);
    System.out.println("The Invaded city is displaying...");
    display(ct, true);
    update(ct,true);
    System.out.println("The Updated city is displaying...");
    display(ct, true);    
  }//main method
  
  //builds city
  public static int[][] buildCity(int row, int column, boolean form){
    int[][] cityValue = new int[row] [column]; 
    int counter = 1;
    for (int i = 0; i < cityValue.length; i++){
      for (int j = 0; j < cityValue[i].length; j++){
        //make rand 
        Random num = new Random();
        int block = num.nextInt(899) + 100;
        cityValue[i][j] = block;
        counter++;
      
      }
    }
     return cityValue;
  }//build city method
  
  //prints city
  //public static void cityArray(int[][] cityValue, boolean form){
    //for (int i = 0; i < cityValue.length; i++){
        //for (int j = 0; j < cityValue[i].length; j++){
          //System.out.print(cityValue[i][j] + " ");
        //}
      //System.out.println();

    //}  
  //}
  
  public static void display(int[][] cityValue, boolean form){
     for (int i = 0; i < cityValue.length; i++){
        for (int j = 0; j < cityValue[i].length; j++){
    System.out.printf("%5d", cityValue[i][j]);
        }
       System.out.println();
     }
  }//display method
  
  //public static void invade(int[][] cityValue, boolean form){
     //for (int i = 0; i < cityValue.length; i++){
       // for (int j = 0; j < cityValue[i].length; j++){
    
     //   }
       
     //}
  //}//invade method
  
///////////////////////////////
  public static int[][] invade(int[][] cityValue, boolean form){
    
    Random numInvaded = new Random();
    int k=numInvaded.nextInt(10);
    for(int counter = 0; counter<k; counter++){
      for (int i = 0; i < cityValue.length; i++){
        for (int j = 0; j < cityValue[i].length; j++){
        //make rand 
          Random order = new Random();
          int o= order.nextInt(cityValue.length);
          int p= order.nextInt(cityValue[0].length);
          cityValue[o][p]=-(cityValue[o][p]);      
        }
    }
  }

     return cityValue;
  }//build city method
///////////////////////////////
  public static void update(int[][] cityValue, boolean form){
     for (int i = cityValue.length-1; i>=0; i--){
        for (int j = cityValue[i].length-1;j>=0; j--){
          if(cityValue[i][j]<0){
            cityValue[i][j]=Math.abs(cityValue[i][j]);
            if(j<cityValue[i].length-1){
               cityValue[i][j+1]=-(cityValue[i][j+1]);  
            }
          }
    
        }
       
     }
  }//update method  
  
  
}