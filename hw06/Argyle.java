//CSE002 Shenyi Yu 3/20/2018 This document will generate argyle pattern
import java.util.Scanner; //import an instance of the class
public class Argyle {
  public static void main(String[] args){
    Scanner myScanner; // declaration
    myScanner = new Scanner (System.in); //construction statement

//please enter a positive integer for the width of Viewing window in characters.  80
//please enter a positive integer for the height of Viewing window in characters.  30
//please enter a positive integer for the width of the argyle diamonds.  8
//please enter a positive odd integer for the width of the argyle center stripe.  3(must be odd and not larger than half of the diamond size. )
//please enter a first character for the pattern fill.  .
//please enter a second character for the pattern fill.  +
//please enter a third character for the stripe fill. 
    
    boolean wv= false;
    int WidthOfViewingWindow=0;//ask for  a positive integer for the width of Viewing window in characters
    
    boolean hv=false;
    int HeightOfViewingWindow=0;//ask for a positive integer for the height of Viewing window in characters
    
    boolean wd= false;
    int WidthOfDiamond=0;//ask for a positive integer for the width of the argyle diamonds
    boolean ws= false;
    int widStripe=0;//ask for a positive odd integer for the width of the argyle center stripe
		
	//System.out.print("please enter a first character for the pattern fill.");//prompt the user for the tip percentage
  //String temp1 = myScanner.next();
	//char result1 = temp1.charAt(0);
  //System.out.print("please enter a second character for the pattern fill.");//how many people to split
  //String temp2 = myScanner.next();
	//char result2 = temp2.charAt(0); 
  //System.out.print("please enter a third character for the pattern fill.");//how many people to split
  //String temp3 = myScanner.next();
	//char result3 = temp3.charAt(0); 
  
	
	

////width of Viewing window
	
	while (wv==false){//make a while loop to test the input
    System.out.println("please enter a positive integer for the width of Viewing window in characters.");
    wv= myScanner.hasNextInt();//test if it is integer
    if (wv==true){
      WidthOfViewingWindow= myScanner.nextInt();//accept user input
    }
    else{
        String junkWord = myScanner.next(); //    
    } 
      if(WidthOfViewingWindow>0){
        System.out.println("the width of Viewing window in characters is "+WidthOfViewingWindow);
        }//if the input is positive                              
      else{
        wv=false;
      }//else
  } //while number ==false  

	
	

////height of Viewing window
	
	  while (hv==false){//make a while loop to test the input
    System.out.println("please enter a positive integer for the height of Viewing window in characters.");
    hv= myScanner.hasNextInt();//test if it is integer
    if (hv==true){
      HeightOfViewingWindow= myScanner.nextInt();//accept user input
    }
    else{
        String junkWord = myScanner.next(); //    
    } 
      if(HeightOfViewingWindow>0){
        System.out.println("the height of Viewing window in characters is "+HeightOfViewingWindow);
        }//if the input is positive                               
      else{
        hv=false;
      }//else
  } //while number ==false  	
	
	
  
	
////the width of the argyle diamonds
	
	  while (wd==false){//make a while loop to test the input
    System.out.println("please enter a positive integer for the width of the argyle diamonds.");
    wd= myScanner.hasNextInt();//test if it is integer
    if (wd==true){
      WidthOfDiamond= myScanner.nextInt();//accept user input
    }
    else{
        String junkWord = myScanner.next(); //    
    } 
      if(WidthOfDiamond>0){
        System.out.println("the width of the argyle diamonds is "+WidthOfDiamond);
        }//if width of the argyle diamonds is positive                                
      else{
        wd=false;
      }//else
  } //while number ==false  	
	

////the width of the argyle center stripe  

	  while (ws==false){//make a while loop to test the input
    System.out.println("please enter a positive odd integer for the width of the argyle center stripe.");
    ws= myScanner.hasNextInt();//test if it is integer
    if (ws==true){
      widStripe= myScanner.nextInt();//accept user input
    }
    else{
        String junkWord = myScanner.next(); //    
    } 
      if(widStripe>0 && widStripe<WidthOfDiamond/2 && widStripe%2==1){//if input is positive
			System.out.println("the width of the argyle center stripe is "+widStripe);}//if: positive
			else{
        ws=false;
      }//else: negative
  }
		
	System.out.print("please enter a first character for the pattern fill.");//prompt the user for the tip percentage
  String temp1 = myScanner.next();
	char result1 = temp1.charAt(0);
  System.out.print("please enter a second character for the pattern fill.");//how many people to split
  String temp2 = myScanner.next();
	char result2 = temp2.charAt(0); 
  System.out.print("please enter a third character for the pattern fill.");//how many people to split
  String temp3 = myScanner.next();
	char result3 = temp3.charAt(0); 	


    
        for (int i = 0; i < (HeightOfViewingWindow); i++){   // The outer loop to control the number of the lines the displayed
            for(int j = 0; j < (WidthOfViewingWindow); j++){   //The inner loop to control output on each line
              if ( (j % (WidthOfDiamond*2)) <= i % (WidthOfDiamond*2) + widStripe/2 && (j % (WidthOfDiamond*2)) >= i % (WidthOfDiamond*2) - widStripe / 2){
                System.out.print(result3);
              } else if ( (j % (WidthOfDiamond*2)) <= WidthOfDiamond*2 - i % (WidthOfDiamond*2) - 1 + widStripe / 2 && (j % (WidthOfDiamond*2)) >= WidthOfDiamond * 2 - i % (WidthOfDiamond*2) - 1 -widStripe / 2 ) {
                System.out.print(result3);
              } else if ( (j % (WidthOfDiamond*2)) <= WidthOfDiamond + (i % (WidthOfDiamond*2)) % WidthOfDiamond - 1 && (j % (WidthOfDiamond*2)) > WidthOfDiamond - (i % (WidthOfDiamond*2)) % WidthOfDiamond - 1 && (i % (WidthOfDiamond*2)) < WidthOfDiamond ){
                System.out.print(result2);
              } else if ( (j % (WidthOfDiamond*2)) >= (i % (WidthOfDiamond*2)) % WidthOfDiamond && (j % (WidthOfDiamond*2)) <= WidthOfDiamond *2 - (i % (WidthOfDiamond*2)) % WidthOfDiamond - 1 && (i % (WidthOfDiamond*2)) >= WidthOfDiamond ) {
                System.out.print(result2);
              } else {
                System.out.print(result1);
              }
    }
                System.out.println();
  }
	
	
	}// end of the main method
}// end of the class       
 