public class Disarray{
  public static int[] copy(int[] array){
    
    int[] newArray;
    newArray = new int[array.length];
    
    for(int i = 0; i < array.length; i++){
      newArray[i] = array[i];
    }// for loop
    
    return newArray;
    
  }// end of copy method
  
  public static void inverter(int[] array){
    int temp = 0;
    for(int k = 0; k < array.length/2 ; k++){
      temp = array[array.length - k - 1];
      array[array.length - k - 1] = array[k];
      array[k] = temp;
    }// for loop
    
  }// end of inverter method
  
  public static int[] flip(int[] array){
    
    int[] flipArray = copy(array);
    
    inverter(flipArray);
    
    return flipArray;
    
  }// end of flip method
  
  public static void print(int[] array){
    System.out.print("{ ");
    for(int j = 0; j < array.length; j++){
      System.out.print(array[j] + ", ");
    }// for loop
    System.out.print("}");
  }// end of print method
  
  public static void main(String[] args){
    int[] array0 = { 1, 2, 3, 5, 8, 13, 21, 34 };
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    //6
    inverter(array0);
    print(array0);
    System.out.println(" ");
    //7
    flip(array1);
    print(array1);
    System.out.println(" ");
    //8
    int[] array3 = flip(array2);
    print(array3);
    System.out.println(" ");
  }// end of main method
}// end of class

