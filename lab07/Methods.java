//Shenyi Yu
//CSE2
//This program is designed to randomly generating grammatically correct English sentences.
import java.util.Scanner;
import java.util.Random;
public class Methods {
  
  public static String adjs () {            //first Method for adjectives
    Random randomGenerator = new Random();        //declare and construct the instance 
    int randomInt = randomGenerator.nextInt(10);  //randomly generate an integer between 0 to 9
    String adjective = " ";
    switch (randomInt) {                         
      case 0:
        adjective = "optimistic";
        break;
      case 1:
        adjective = "independent";
        break;
      case 2:
        adjective = "outgoing";
        break;
      case 3:
        adjective = "active";
        break;
      case 4:
        adjective = "aggressive";
        break;
      case 5:
        adjective = "ambitious";
        break;
      case 6:
        adjective = "careful";
        break;
      case 7:
        adjective = "candid";
        break;
      case 8:
        adjective = "dedicated";
        break;
      case 9:
        adjective = "diplomatic";
        break;
    }
    return adjective;  
  }
  
  public static String adj2 () {            //second method for adjectives
    Random randomGenerator = new Random();         //declare and construct the instance
    int randomInt = randomGenerator.nextInt(10);   //randomly generate an integer between 0 to 9
    String adjective2 = " ";
    switch (randomInt) {
      case 0:
        adjective2 = "efficient";
        break;
      case 1:
        adjective2 = "energetic";
        break;
      case 2:
        adjective2 = "faithful";
        break;
      case 3:
        adjective2 = "frank";
        break;
      case 4:
        adjective2 = "generous";
        break;
      case 5:
        adjective2 = "humorous";
        break;
      case 6:
        adjective2 = "impartial";
        break;
      case 7:
        adjective2 = "industrious";
        break;
      case 8:
        adjective2 = "ingenious";
        break;
      case 9:
        adjective2 = "intelligent";
        break;
    }
    return adjective2;  
  }
  
  public static String nounSubjects () {            //third method for the subjects
    Random randomGenerator = new Random();          //declare and construct the instance
    int randomInt = randomGenerator.nextInt(10);    //randomly generate an integer between 0 to 9
    String nounSubject = " ";
    switch (randomInt) {
      case 0:
        nounSubject = "worker";
        break;
      case 1:
        nounSubject = "teacher";
        break;
      case 2:
        nounSubject = "horse";
        break;
      case 3:
        nounSubject = "car";
        break;
      case 4:
        nounSubject = "dog";
        break;
      case 5:
        nounSubject = "panda";
        break;
      case 6:
        nounSubject = "Eva";
        break;
      case 7:
        nounSubject = "baby";
        break;
      case 8:
        nounSubject = "cat";
        break;
      case 9:
        nounSubject = "student";
        break;
      }
      return nounSubject;
  }
    
  
    public static String pastVerbs () {            //fourth method for the past tense verb used in first sentence
    Random randomGenerator = new Random();         //declare and construct the instance
    int randomInt = randomGenerator.nextInt(10);   //randomly generate an integer between 0 to 9
    String pastVerb = " ";
    switch (randomInt) {
      case 0:
        pastVerb = "added";
        break;
      case 1:
        pastVerb = "alerted";
        break;
      case 2:
        pastVerb = "amused";
        break;
      case 3:
        pastVerb = "felt";
        break;
      case 4:
        pastVerb = "loved";
        break;
      case 5:
        pastVerb = "asked";
        break;
      case 6:
        pastVerb = "hated";
        break;
      case 7:
        pastVerb = "advised";
        break;
      case 8:
        pastVerb = "thought";
        break;
      case 9:
        pastVerb = "resisted";
        break;
      }
      return pastVerb;
  }
  
  public static String nounObjects () {             //the fifth method for the objects who receive the actions done by the subjects
    Random randomGenerator = new Random();          //declare and construct the instance
    int randomInt = randomGenerator.nextInt(10);    //randomly generate an integer between 0 to 9
    String nounObject = " "; 
    switch (randomInt) {
      case 0:
        nounObject = "Alicia";
        break;
      case 1:
        nounObject = "Krystal";
        break;
      case 2:
        nounObject = "Angel";
        break;
      case 3:
        nounObject = "Christine";
        break;
      case 4:
        nounObject = "Jack";
        break;
      case 5:
        nounObject = "Emma Stone";
        break;
      case 6:
        nounObject = "Owen";
        break;
      case 7:
        nounObject = "Riri";
        break;
      case 8:
        nounObject = "servant";
        break;
      case 9:
        nounObject = "Diany";
        break;
      }
      return nounObject;
  }
    
    public static String tools () {                  //sixth method for the tools the selected subject uses
    Random randomGenerator = new Random();           //declare and construct the instance
    int randomInt = randomGenerator.nextInt(10);     //randomly generate an integer between 0 to 9
    String nounTool = " ";
    switch (randomInt) {
      case 0:
        nounTool = "scissors";
        break;
      case 1:
        nounTool = "a pencil";
        break;
      case 2:
        nounTool = "a kettle";
        break;
      case 3:
        nounTool = "a straw";
        break;
      case 4:
        nounTool = "a ring";
        break;
      case 5:
        nounTool = "a towel";
        break;
      case 6:
        nounTool = "an eraser";
        break;
      case 7:
        nounTool = "a piece of advice";
        break;
      case 8:
        nounTool = "a pill";
        break;
      case 9:
        nounTool = "a laptop";
        break;
      }
      return nounTool;
  }
    
    public static String verbs () {                 //seventh method for verb
    Random randomGenerator = new Random();          //declare and construct the instance
    int randomInt = randomGenerator.nextInt(10);    //randomly generate an integer between 0 to 9
    String verb = " ";
    switch (randomInt) {
      case 0:
        verb = "deny";
        break;
      case 1:
        verb = "blow";
        break;
      case 2:
        verb = "chase";
        break;
      case 3:
        verb = "insert";
        break;
      case 4:
        verb = "break";
        break;
      case 5:
        verb = "shake";
        break;
      case 6:
        verb = "put";
        break;
      case 7:
        verb = "enjoy";
        break;
      case 8:
        verb = "knock";
        break;
      case 9:
        verb = "dislike";
        break;
      }
      return verb;
  }
      
    public static String adverbs () {                //the eighth method for adverb
    Random randomGenerator = new Random();           //declare and construct the instance
    int randomInt = randomGenerator.nextInt(10);     //randomly generate an integer between 0 to 9
    String adverb = " ";
    switch (randomInt) {
      case 0:
        adverb = "easily";
        break;
      case 1:
        adverb = "rapidly";
        break;
      case 2:
        adverb = "significantly";
        break;
      case 3:
        adverb = "sincerely";
        break;
      case 4:
        adverb = "politely";
        break;
      case 5:
        adverb = "hopefully";
        break;
      case 6:
        adverb = "intensely";
        break;
      case 7:
        adverb = "precisely";
        break;
      case 8:
        adverb = "profoundly";
        break;
      case 9:
        adverb = "probably";
        break;
      }
      return adverb;
  }
    
    public static String objects () {                 //the ninth method for the objects
    Random randomGenerator = new Random();            //declare and construct the instance
    int randomInt = randomGenerator.nextInt(10);      //randomly generate an integer between 0 to 9
    String object = " ";
    switch (randomInt) {
      case 0:
        object = "blueberry";
        break;
      case 1:
        object = "watermelon";
        break;
      case 2:
        object = "lemon";
        break;
      case 3:
        object = "wardrobes";
        break;
      case 4:
        object = "dining table";
        break;
      case 5:
        object = "garden";
        break;
      case 6:
        object = "tissues";
        break;
      case 7:
        object = "pencils";
        break;
      case 8:
        object = "iPads";
        break;
      case 9:
        object = "stones";
        break;
      }
      return object;
  }
 
    public static String thesis() {                           //the tenth method for the first sentence also the thesis
    
    Scanner myScanner = new Scanner(System.in);               //declare and construct Scanner instance 
    String nSub;                                              //declare nSub as a string variable
    while (true) {                                            //this while loop will keep running until the break statement
      
    String adj1 = adjs();                               // calling the method one
    String adj2 = adjs();                               // calling the method one again
    nSub = nounSubjects();                                    // calling the method three 
    String pastV = pastVerbs();                               // calling the method four
    String nObj = nounObjects();                              // calling the method five
    String thesisSen = "The " + adj1 + " " + nSub + " " + pastV + " " + "the " + adj2 + " " + nObj + "."; // the first sentence also the thesis
    System.out.println (thesisSen);  // print out the sentence
    System.out.println ("Do you want another sentence? (yes OR no)");  // prompts the user to answer whether he wants another sentence or not
    String answer = myScanner.next();  // where the user inputs the answer corresponding to the question above
      if ((answer).equals("yes")) {    // if the user types in "yes" exactly, the if statement will run
        continue;                      // skip the rest and go back to the top of while loop and start over again
      }
        else {                         // we will exit the while loop when the answer is not a yes
          break;                       
        }
    }   
        return nSub;                   // return the subject in the sentence we select to the other sentences
    }
  
    public static String sentences () {          //the tenth method for the action sentences and conclusion
    Scanner myScanner = new Scanner(System.in);  //declare and construct Scanner instance
    String adj1 = adjs();  //calling method one               
    String adj2 = adjs();  //calling method one      
    String nSub = thesis();      //calling method ten
    String pastV = pastVerbs();  //calling four
    String nObj = nounObjects(); //calling method five
    String adv = adverbs();      //calling method eight 
    String adj22 = adj2();//calling method two
    String tool = tools();        //calling method six
    String nObjs = objects();    //calling method nine
    String vb = verbs();         //calling method seven
    
    String sen1 = "The " + nSub + " was " + adj22 + " " +  adv+ " to " + adj1 + " " + nObj + "."; //  assigning sentence one to variable sen1
    String sen2 = nSub + " used " + tool + " to " + vb + " " + nObjs + " at the " + adj2 + " " + nObjs + "."; //  assigning sentence two to variable sen2
    String conclusion = "That " + nSub + " " + pastV + " his " + tool + "."; // assigning the conclusion to variable conclusino
    
    return (sen1 + "\n" + sen2 + "\n" + conclusion);   //return the rest sentences
  }
     
  public static void main(String[] args) {             //the main method
    System.out.println(sentences());                   // print out the whole thing 
  } 
     
   
   
  }


