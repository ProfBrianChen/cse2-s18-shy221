//CSE002 Shenyi Yu 3/27/2018
//This program is designed to process a string by examining all the characters, or just a specified number of characters in the string, and determining if they are letters.

import java.util.Scanner; //import Scanner instance to the class 

public class StringAnalysis {
  
    public static void main(String[] args) { //the main method
   Scanner myScanner = new Scanner(System.in); //declare and construct Scanner instance
    
    String testString = " "; //initialization
    int charNum = 0;   //initialization
   
   System.out.println("Do you want to: (a)examine all the characters, OR (b)specified number of characters: answer a OR b ");
   //prompts the user to choose which way do they want to examine
   String answer = myScanner.next();  //the user inputs the answer
   if ((answer).equals("a")) {        //runs if the user chooses the first one
      boolean answera = method(testString);  //call the method that only accepts a string
      System.out.println(answera);          //print out the boolean result
   }
    if ((answer).equals("b")) {      //runs if the user chooses the second one
      boolean answerb = method(testString, charNum);  //call the method that accepts a string and an integer
      System.out.println(answerb);   //print out the boolean result
    }
    
    } //end of the main method
    
    public static boolean method(String testString) {//the method that explain the whole string
    Scanner myScanner = new Scanner(System.in);   //declare and construct Scanner instance
    System.out.println("Type in a string to examine: "); //prompts the user to type in a String 
    testString = myScanner.next();   //where user types in the string
     
      boolean checkWhole; //declare boolean variable checkWhole for checking the whole string
      for (int j = 0; j < ((int) testString.length()); j++) { //this while loop will keep running when j is less than the string length
      char letter1 = testString.charAt(j);    //the letter at j position in the string
      if (letter1 >= 'a' && letter1 <= 'z'){ //when letter1 is a letter the if statement will run
        checkWhole = true;         //checkWhole is true when letter1 is a letter 
      } 
      else {
        checkWhole = false;    //otherwise it would be false
           }
      
      if(checkWhole == false) {          //if checkWhole is false
         return checkWhole; //we will return boolean variable checkWhole which is false here
      }
        else{
        continue; //otherwise we will examine the next character
        } 
      }
      return true; //return true when checkWhole has never been false
      }
      
    
    public static boolean method(String testString, int charNum) {  //the method that examine a specified number of characters in the string
    
    Scanner myScanner = new Scanner(System.in);  //declare and construct Scanner instance
      
      System.out.println("Type in a string that you want to examine: "); //prompts the user to type in a string
      testString = myScanner.next();  //where user types in the string
      
      boolean checkSpecified; //declare variable checkSpecified for checking a specified number of characters
    
        System.out.print("Please input the number of characters you want to examine: "); //prompts the user to input the number of characters he wants to examine

         while (!myScanner.hasNextInt()) {   //when the input is not an integer the while loop will keep running
         System.out.println("you input the wrong type!"); //indicates Error
         System.out.print("Please input the number of characters you want to examine: "); //prompts the user to input again
         myScanner.next(); //erase the old input and replace it with a new input typed by the user
         }  
         charNum = myScanner.nextInt(); //tells you how many characters to check
         
         
         for (int i = 0; i < charNum; i++) { //the for loop will keeping running until i is greater or equal to the specified number of characters
          char letter2 = testString.charAt(i);    //the letter2 is the character at i position in the string 
          if (letter2 >= 'a' && letter2 <= 'z'){ //when letter2 is a letter the if statement will run
            checkSpecified = true; //when letter2 is a letter checkSpecified is true
          } 
          else {checkSpecified = false; //otherwise it is false
               }
           
          if(checkSpecified == false) { //if checkSpecified is false, return checkSpecified.
          return checkSpecified;
          }
            else{continue; //otherwise we will examine the next character 
            } 
         }         
         
      return true; //return true when checkSpecified has never been false
      }
  
  
  
}