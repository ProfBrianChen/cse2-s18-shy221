//CSE002 Shenyi Yu 3/27/2018 
//This program is designed to calculate the area of three different shapes; a rectangle, a triangle and a circle.
 
import java.util.Scanner; //import Scanner instance to the class 
public class Area {         
  
  public static double triangle(double heiTriangle, double baseTriangle) { //The method for calculating the area of the triangle
    double areaTriangle = (heiTriangle*baseTriangle)/2;   //formula for calculating the area of a triangle
    return areaTriangle; //return the value of the area
  } //end of the method
  
  public static double rectangle(double heiRect, double widthRect) { //The method for calculating the area of the rectangle
    double areaRect = heiRect*widthRect; //formula for calculating the area of a rectangle
    return areaRect; //return the value of the area
  } //end of the method
  
  public static double circle(double radius) {  //The method for calculating the area of the circle
    double areaCircle = (Math.pow(radius,2)*(Math.PI)); //formula for calculating the area of a circle
    return areaCircle; //return the value of the area
  } //end of the method
  
  
  public static void main(String[] args) {   //the main method for checking all the inputs
   Scanner myScanner = new Scanner(System.in); //declare and construct Scanner instance
   
    System.out.println("Choose one type from the following: triangle OR circle OR rectangle: ");
    //asks the user to input a shape that they want
    String shape = myScanner.next(); //user inputs the name in string 
    while (!(shape).equals("triangle") && !(shape).equals("circle") && !(shape).equals("rectangle")) {
      //this while loop will keep running until the input is acceptable
      System.out.println("The input is not acceptable, please enter again.");
      //indicates user needs to input a correct string
      shape = myScanner.next(); //erases the old input and lets the user input again
    }
    System.out.println("Enter the values of the shape."); //prompts the user to enter the dimensions of the shape they choose
    
    if ((shape).equals("triangle")) { //runs if the input is triangle
      System.out.print("Height: ");   //prompts the user to input the height 
        
        while (!myScanner.hasNextDouble()) {      //this while loop will keep running when the input is not a double
          System.out.println("Error: try again"); //shows error
          System.out.print("Height: ");           //prompts the user to input again
          myScanner.next();                       
        }
        double heiTriangle = myScanner.nextDouble(); //declare the variable and assign the input value to it

          System.out.print("Length of the base: "); //prompts the user to input the length of the base
        while (!myScanner.hasNextDouble()) {        //this while loop will keep running when the input is not a double
          System.out.println("Error: try again"); //shows error
          System.out.print("Length of the base: ");           //prompts the user to input again
          myScanner.next();                         //erases the old input and lets the user input again
        }
      double baseTriangle = myScanner.nextDouble(); //declare the variable and assign the input value to it
      
      double areaTriangle = triangle(heiTriangle, baseTriangle);         //call the method to get the area and assign the value to this new variable
      System.out.println("The area of the triangle is: " + areaTriangle); //print out the area of the triangle
    }
    
    if ((shape).equals("circle")) {               //runs if the input is circle
      System.out.print("Radius: ");               //prompts the user to input the radius
        while (!myScanner.hasNextDouble()) {      //this while loop will keep running when the input is not a double
  
          System.out.println("Error: try again"); //shows error
          System.out.print("Radius: ");           //prompts the user to input again
          myScanner.next();                       //erases the old input and lets the user input again
        }
    double radius = myScanner.nextDouble(); //declare the variable and assign the input value to it
      
    double areaCircle = circle(radius);     //call the method to get the area and assign the value to this new variable
    System.out.println("The area of the circle is: " + areaCircle); //print out the area of the circle
    }
    
    if ((shape).equals("rectangle")) {            //runs if the input is rectangle
    System.out.print("Height: ");                 //prompts the user to input the height
        while (!myScanner.hasNextDouble()) {      //this while loop will keep running when the input is not a double
          System.out.println("Error: try again"); //shows error
          System.out.print("Height: ");           //prompts the user to input again
          myScanner.next(); //erases the old input and lets the user input again
        }
    double heiRect = myScanner.nextDouble();   //declare the variable and assign the input value to it
    
    System.out.print("Width: ");                  //prompts the user to input the width
        while (!myScanner.hasNextDouble()) {      //this while loop will keep running when the input is not a double
          System.out.println("Error: try again"); //shows error
          System.out.print("Width: ");           //prompts the user to input again
          myScanner.next(); //erases the old input and lets the user input again
        }
    double widthRect = myScanner.nextDouble(); //declare the variable and assign the input value to it
      
    double areaRect = rectangle(heiRect, widthRect);   //call the method to get the area and assign the value to this new variable
    System.out.println("The area of the rectangle is: " + areaRect);  //print out the area of the rectangle
    }
    
    
    
  } //end of the main method
}  //end of the class