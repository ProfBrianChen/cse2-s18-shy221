//Shenyi Yu
//CSE002
//April 17 2018
//This program is designed to prompt the user to determine which player wins according to their hands of cards,
//search for the pair, three of a kind, flush, full house using different methods, give outputs as boolean 
//and print out whether it was found or not and how many iterations taken.
import java.util.Random;
import java.util.Arrays; 
public class DrawPoker{
  //the main method
  public static void main(String[] args) {
    //declare the array as the poker cards with length of 52
    final int NUM = 52;
    int[] cards = new int[NUM];
    for(int i=0; i < 52; i++){
      cards[i] = i;
    }//for loop to get the 52 cards completely as poker 
    
      for (int i=0; i<cards.length; i++) {
      //find a random member to swap with
      int target = (int) (cards.length * Math.random() );
      //swap the values
      int temp = cards[i];
      cards[i] = cards[target];
      cards[target] = temp;
    }//for loop to shuffle the poker so that it is randomly put
    
    int[]A=new int[5];//assign hands as array to each player
    int[]B=new int[5];//assign hands as array to each player
    A[0]=cards[0];
    A[1]=cards[2];
    A[2]=cards[4];
    A[3]=cards[6];
    A[4]=cards[8];
    B[0]=cards[1];
    B[1]=cards[3];
    B[2]=cards[5];
    B[3]=cards[7];
    B[4]=cards[9];
    //cards are assigned to each hand of the two players now randomly
    //printing out each player's hand     
    System.out.print("The hand of the first player: "); 
    for (int i = 0; i < number(A).length; i++){
      System.out.print(number(A)[i] + " ");
    } //printing out the first player's hand
          System.out.println(" ");
    
    System.out.print("The hand of the second player:");
    for (int i = 0; i < number(B).length; i++){
      System.out.print(number(B)[i] + " ");
    } //for loop to printing out the second player's hand 
          System.out.println(" ");
    
    
    
    
    //printing out the results of each players hand, using the methods to determine whether they are true or false 
    //checking for pairs, three of a kind, flush, full house 
    
    System.out.println("Does the first player have any pairs: " + pair(A));
    System.out.println("Does the second player have any pairs: " + pair(B));
    
    System.out.println("Does the first player have three of a kind: " + threeofakind(A));
    System.out.println("Does the second player have three of a kind: " + threeofakind(B));

    System.out.println("Does the first player have a flush: " + flush(A));
    System.out.println("Does the second player have a flush: " + flush(B));
    
        
    System.out.println("Does the first player have a full house: " + fullhouse(A));
    System.out.println("Does the second player have a full house: " + fullhouse(B));
    
    
    //determining which player has a highesr ranking card 
    
    int highestRankA = highestRank(A); 
    int highestRankB = highestRank(B); 
    if (highestRankA > highestRankB){
      System.out.println("the first player has the highest ranking card.");
    } // if statement 
    else if (highestRankB > highestRankA){
      System.out.println("the second player has the highest ranking card.");
    } //else if  statement
    else {
      System.out.println("Both players have the same highest ranking card.");
    }
    
    //deciding who is the winner 
    
    String winner = ""; 
    
    if (fullhouse(A) != fullhouse(B)){
      if (fullhouse(A) == true){
      winner = "the first player"; 
    }
    else if (fullhouse(B) == true){
      winner = "the second player";
    }
    }//highest hand : full house
    
    if (flush(A) != flush(B)){
      if (flush(A) == true){
      winner = "the first player"; 
    }
    else if (flush(B) == true){
      winner = "the second player";
    }
    } //second highest hand: flush 
    
    if (threeofakind(A) != threeofakind(B)){
      if (threeofakind(A) == true){
      winner = "the first player"; 
    }
    else if (threeofakind(B) == true){
      winner = "the second player";
    }
    } //third highest hand: three of a kind
    
    
    if (pair(A) != pair(B)){
      if (pair(A) == true){
      winner = "the first player"; 
    }
    else if (pair(B) == true){
      winner = "the second player";
    }
    }
    else{
      
        if (highestRankA > highestRankB){
      winner = "the first player";
    }// if statement 
      else if (highestRankB > highestRankA){
      winner = "the second player";  
    } //else if  statement
      else {
      winner = "There is no winner, they have equal hands";
    }
      } //fourth highest hand: pair, and if not, highest ranking card wins 
    
    //printing out the winner of the game
    
    System.out.println("The winner of this game is: " + winner); 
    
    
    
    
    
    
  }//main method 
  
  //justifying numbers into the name of the card in the same color
  public static String [] number (int [] hand){
    String [] number = new String [5];
    for (int i = 0; i < number.length; i++){
      
      if (hand[i] % 13 == 0){
        number [i] = "Ace";
      }
      if (hand[i] % 13 == 1){
        number [i] = "2";
      }
      if (hand[i] % 13 == 2){
        number [i] = "3";
      }
      if (hand[i]% 13 == 3){
        number [i] = "4";
      }
      if (hand[i]% 13 == 4){
        number [i] = "5";
      }         
      if (hand[i]% 13 == 5){
        number [i] = "6";
      }         
      if (hand[i]% 13 == 6){
        number [i] = "7";
      }         
      if (hand[i]% 13 == 7){
        number [i] = "8";
      }         
      if (hand[i]% 13 == 8){
        number [i] = "9";
      }         
      if (hand[i]% 13 == 9){
        number [i] = "10";
      }         
      if (hand[i]% 13 == 10){
        number [i] = "Jack";
      }         
      if (hand[i]% 13 == 11){
        number [i] = "Queen";
      }
     if (hand[i]% 13 == 12){
        number [i] = "King";
      } 
      
    } //for loop to determine which card is referred to by the number
         
    return number; 
    
         } //number method to assign number to all of the cards in the players' hands 
  //pair method to check for pairs
public static boolean pair(int[] hand){
  boolean p;
  int count = 0;
  if(hand[0]%13==hand[1]%13||
     hand[0]%13==hand[2]%13||
     hand[0]%13==hand[3]%13||
     hand[0]%13==hand[4]%13||
     hand[1]%13==hand[2]%13||
     hand[1]%13==hand[3]%13||
     hand[1]%13==hand[4]%13||
     hand[2]%13==hand[3]%13||
     hand[2]%13==hand[4]%13||
     hand[3]%13==hand[4]%13){
    count += 1; //incrementing the count by 1 if there is a pair 
  }//if loop to check if there is a pair
 if(count>0)
 {
  p=true; 
 }//if true
  else{
    p=false;
  }//else false
  return p;
  }
  //three of a kind method to check for three cards of same number but in different colors
public static boolean threeofakind(int[] hand){
  boolean t;
  int count =0;
  int x = 2; 
  int C1 = hand[0] %13;
  int C2 = hand[1] %13;   
  int C3= hand[x]%13;
    for (C3 = hand[x] % 13; x < hand.length; x++){
      if (C1 == C2 && C2 == C3){
        count++;
      }
    }
  
   x = 3; 
     C1 = hand[0] %13;
     C2 = hand[2] %13; 
    
    for (C3 = hand[x] % 13; x < hand.length; x++){
      if (C1 == C2 && C2 == C3){
        count ++;
      }
    }

     C1 = hand[0] %13;
     C2 = hand[3] %13; 
      x=4;
     C3 = hand[4] % 13;
    
    if (C1 ==C2 && C2 == C3){
      count++;
    }

     x = 3; 
     C1 = hand[1] %13;
    C2 = hand[2] %13; 
    
    for (C3 = hand[x] % 13; x < hand.length; x++){
      if (C1 == C2 && C2 == C3){
        count++;
      }
    }

   C1 = hand[1] %13;
   C2 = hand[3] %13; 
  x=4; 
  C3 = hand[4] % 13;
  if (C1 == C2 && C2 == C3){
    count++;
    }
    
  C1 = hand[2] %13;
   C2 = hand[3] %13;  
 C3 = hand[4] % 13;  
  if (C1 == C2 && C2 == C3){
      count++;
    }
  if(count>0)
   {t=true; }//if true
  else{t=false;}//else false
  return t;
  }//three of a kind method
    
public static boolean flush(int []hand){
    boolean f;
    if (hand[0]/13 == hand[1]/13 && hand[1]/13 == hand[2]/13 && hand[2]/13 == hand[3]/13 && hand[3]/13 == hand[4]/13){
      f = true;
    }
    else {
      f = false;
    }
    return f;
  } //flush method 
  public static boolean fullhouse(int[] hand){
    boolean result;
    if (hand[0] % 13 == hand[1] % 13 &&
      hand[1] % 13 == hand[2] % 13 &&
      hand[3] % 13 == hand [4] % 13 ||
        hand[0] % 13 == hand[1] % 13 &&
        hand[1] % 13 == hand[3] % 13 &&
        hand[2] % 13 == hand [4] % 13 ||
         hand[0] % 13 == hand[1] % 13 &&
         hand[1] % 13 == hand[4] % 13 &&
         hand[3] % 13 == hand [2] % 13 ||
          hand[0] % 13 == hand[2] % 13 &&
          hand[3] % 13 == hand[2] % 13 &&
          hand[1] % 13 == hand [4] % 13 ||
            hand[0] % 13 == hand[2] % 13 &&
            hand[4] % 13 == hand[2] % 13 &&
            hand[3] % 13 == hand [1] % 13 ||
            hand[0] % 13 == hand[3] % 13 &&
            hand[3] % 13 == hand[4] % 13 &&
            hand[1] % 13 == hand [2] % 13 ||
            hand[1] % 13 == hand[2] % 13 &&
            hand[2] % 13 == hand[3] % 13 &&
            hand[0] % 13 == hand [4] % 13 ||
            hand[1] % 13 == hand[2] % 13 &&
            hand[2] % 13 == hand[4] % 13 &&
            hand[0] % 13 == hand [3] % 13 ||
            hand[1] % 13 == hand[3] % 13 &&
            hand[3] % 13 == hand[4] % 13 &&
            hand[2] % 13 == hand [0] % 13 ||  
      hand[2] % 13 == hand[3] % 13 &&
      hand[3] % 13 == hand[4] % 13 &&
      hand[0] % 13 == hand [1] % 13) 
    {
      result = true;
    }
    else {
      result = false;
    }
    
    return result;
    
  } //full house method (three of a kind and a pair)
        
 public static int highestRank(int [] hand){
    
    int[] orderhand = new int[]{hand[0] % 13, hand[1] % 13, hand[2] % 13, hand[3] % 13, hand[4] % 13};
    Arrays.sort(orderhand);
    int highestRank = orderhand[orderhand.length - 1];
    
    return highestRank;
    
  } //method to find the highest rank in a hand 
         
        
 
    
  
    

}//class parentheses
