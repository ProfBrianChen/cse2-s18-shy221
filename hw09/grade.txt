Grade:     95/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
Yes.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
Yes.
C) How can any runtime errors be resolved?
N/A
D) What topics should the student study in order to avoid the errors they made in this homework?
N/A
E) Other comments:
Good job. Think about how you could use loops to minimize the amount of hard coded comparisons you 
have to make.