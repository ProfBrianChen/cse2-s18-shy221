//02.16.2018 CSE002 Yushen Yi This programme aims to prompt the user for picking up a card randomly and print out which card the user picks
import java.util.Random;//import statement
public class CardGenerator{
  // main method required for every Java program
  public static void main(String[] args) {
    Random rand=new Random();
    int card= rand.nextInt(52)+1;//choose a number from 1 to 52 
    if ( card >= 1 && card <= 13 ){
      	if( card==1 ){
		      System.out.println("you pick up the ace of diamonds");
	       }
        if( card==11 ){
		      System.out.println("you pick up the jack of diamonds");
	      }
        if( card==12 ){
		      System.out.println("you pick up the queen of diamonds");
	      }
        if( card==13 ){
		      System.out.println("you pick up the king of diamonds");
	      }
        else {
          System.out.println("you pick up"+ card+ "of diamonds.");
        }
    }
        if ( card >= 14 && card <= 26 ){
      	if( card==14 ){
		      System.out.println("you pick up the ace of clubs");
	       }
        if( card==24 ){
		      System.out.println("you pick up the jack of clubs");
	      }
        if( card==25 ){
		      System.out.println("you pick up the queen of clubs");
	      }
        if( card==26 ){
		      System.out.println("you pick up the king of club");
	      }
        else {
          System.out.println("you pick up"+ (card-12)+ "of diamonds.");
        }
    }
      if ( card >= 27 && card <= 39 ){
      	if( card==27 ){
		      System.out.println("you pick up the ace of hearts");
	       }
        if( card==37 ){
		      System.out.println("you pick up the jack of hearts");
	      }
        if( card==38 ){
		      System.out.println("you pick up the queen of hearts");
	      }
        if( card==39 ){
		      System.out.println("you pick up the king of hearts");
	      }
        else {
          System.out.println("you pick up"+ (card-26)+ "of hearts.");
        }
    }
        if ( card >= 40 && card <= 52 ){
      	if( card==40 ){
		      System.out.println("you pick up the ace of spades");
	       }
        if( card==50 ){
		      System.out.println("you pick up the jack of spades");
	      }
        if( card==51 ){
		      System.out.println("you pick up the queen of spades");
	      }
        if( card==52 ){
		      System.out.println("you pick up the king of spades");
	      }
        else {
          System.out.println("you pick up"+ (card-39)+ "of spades.");
        }
    }

  }//end of main method   
 
}//end of class