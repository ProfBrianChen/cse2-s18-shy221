//Shenyi Yu
//CSE002
//April 10 2018
//This program is designed to prompt the user to enter 15 ints for students' final grades in CSE2,
//search for the grade user entered by using binary search method and linear search method, 
//and print out whether it was found or not and how many iterations taken.

import java.util.Random;  
import java.util.Scanner;
public class CSE2Linear{
  //the main method
  public static void main(String[] args) {
    //declare and construct Scanner instance 
    Scanner myScanner = new Scanner(System.in);
    int i = 0;
    //declare the array students and allocate 15 as the length of the array
    int[] students = new int[15];
    
    System.out.println("Enter 15 increasing ints for final grades in CSE2: ");
    for (i = 0; i < 15; i++) {
      //checking whether the input is an integer or not 
      while (!myScanner.hasNextInt()) {
        System.out.println("Error");
        System.out.println("Please enter an integer: ");
        myScanner.next();
        continue;
      }
      students[i] = myScanner.nextInt();
      //checking whether the input is between 0 to 100
      while (students[i] > 100 || students[i] < 0) {
        System.out.println("Error");
        System.out.println("Please enter an integer between 0 to 100: ");
        students[i] = myScanner.nextInt();
        continue;
      }
      //checking whether the input is greater than the last one
      if (i >= 1) {
        while (students[i] < students[i - 1]) {
          System.out.println("Error");
          System.out.println("Please enter an integer greater or equal to the last one: ");
          students[i] = myScanner.nextInt();
          continue;
        } 
      
      }
      
    }
    //print out the final input array
    for (i = 0; i < 15; i++) {
      System.out.print(students[i] + " ");
    }
    System.out.println();
    //prompts the user to enter a grade to search for
    System.out.print("Enter a grade to search for: ");
    int gradeEntered = myScanner.nextInt();
    //calling the binary search method
    System.out.println(gradeEntered + " was " +BinSearch(students, gradeEntered) );
    //printing out the scrambled grades previously entered by the user
    System.out.println("Scrambled: ");
    //calling the Scramble method 
    Scramble(students);
    System.out.println();
    //prompts the user to enter a grade to search for among the scramble grades
    System.out.println("Enter a grade to search for: ");
    gradeEntered = myScanner.nextInt();
    //calling the linear search method
    System.out.println(gradeEntered + " was " + LinearSearch(students, gradeEntered) );
   
  }  
  /////using binary search to find the grade entered by the user
  public static String BinSearch(int[] students, int gradeEntered){  //return a string 
    
    int left = 0; //smallest index 
    int right = 14; //greatest index
    int counter = 0; //counts iterations
    while (right >= left) { // 
      counter++; //counting how many times this while loop is run (iterations)
      int mid = Math.round((left + right)/2); //if it is even, we choose the higher number as our mid
      if (students[mid] == gradeEntered) {  //if the mid array member is equal to the number entered by the user, boolean variable foundIt is true
        return "The grade is found within " + counter + " iterations";
      }
        else if (students[mid] > gradeEntered) { //if the grade entered belongs to the bottom half, ignore the higher half
          right= mid - 1; 
        }
          else if (students[mid] < gradeEntered) { //if the grade entered belongs to the higher half, ignore the bottom half 
            left = mid + 1;
          }
    }   
      //in the other case, grade entered was not found
      return "The grade is not found in the list within " + counter + " iterations";
  }
    
  ////////using linear search method to find the grade entered among the not sorted (scramble) array members
  public static String LinearSearch(int[] students, int gradeEntered){ 
    int i = 0; 
    int counter = 0; // counts iteraions
  for (i = 0; i < students.length; i++) { //go through every array members
    counter++; //counting how many times this while loop is run (iterations)
    if (students[i] == gradeEntered) { //if the grade entered is equal to the array member, boolean variable foundIt is true
    return "The grade is found within " + counter + " iterations";
    }
  }
    //in the other case, grade entered was not found
    return "The grade is not found within " + counter + " iterations";
  }
    
  /////////scramble the sorted array randomly
  public static void Scramble(int[] students) { 
  
    Random scramble = new Random(); //declare and construct the Random instance
    int i = 0;
    for (i = 0; i < students.length; i++) { 
      //find a random number to swap with
      int target = (int) ( students.length * Math.random() );
      //swap the values
      int swap = students[target]; 
        students[target] = students[i];
        students[i] = swap;
    }
    //print out the swaped values
    for (i = 0; i < 15; i++) { 
      System.out.print(students[i] + " ");
    }
    
  }
    

}
