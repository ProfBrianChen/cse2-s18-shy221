//Shenyi Yu
//CSE002
//April 10 2017
//This program is designed to remove the members of an array by typing in an index and a target element and print out the final array
import java.util.Scanner; 
public class RemoveElements {
  
  //fills the array with random integers and returns the filled array
  public static int[] randomInput(int num[]) {
    int i = 0;
    for (i = 0; i < num.length; i++) {     //assign ramdom integers from 0 to 9 to all the array members
      num[i] = (int) (Math.random() * 10); 
    }
    return num; 
  }
  //delete the member in the position pos off the array 
  public static int[] delete(int[] list, int pos) { 
    //the new array has length 9
    int[] newList = new int[9]; 
    int i = 0;
    //assigning all the values in the original array to the new array except for the member in the position pos
    for (i = 0; i < list.length; i++) { 
      if (i < pos) { 
      newList[i] = list[i];
      }
      else if (i > pos) {
      newList[i - 1] = list[i];
      }
    }
    return newList;
  }
  //deletes all the elements that are equal to target, returning a new list without all those new elements
  public static int[] remove(int[] list, int target) {
    int newLength = 10; 
    int i = 0; //index for the original array
    int j = 0; //index for the new array
    //the new length decreases by one when the for loop finds an element equal to target
    for (i = 0; i < list.length; i++) {
      if (list[i] == target) { 
        newLength--;
      }
    }
    //declare and allocate the new array with new length
    int[] newList = new int[newLength];
    //assigning all the values in the original array to the new array and 
    //remove the members which have the same values as the target 
    for (i = 0, j = 0; i < list.length; i++) {
      if (list[i] == target) { //skip the element which has the same 
        continue; 
      }
      else {
        newList[j] = list[i]; //assign the values from the original array to the new one
        j++;
      }
    }
    return newList;
    
  }
  //the main method
  public static void main(String [] arg){
  //declare and construct the scanner instance
 Scanner scan = new Scanner(System.in);
    //declare and allocate the new array
int num[] = new int[10]; 
int newArray1[]; //the new array after one member is removed
int newArray2[]; //the new array after one member is removed and the elements that have the same value with the target are removed 
int index,target;
int i = 0;
  String out1;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]"); 
    //calling the method that generates 10 ramdom integers
   num = randomInput(num); 
   String out = "The original array is:";
    //printing out the original array plus all the elements
   out += listArray(num);
   System.out.println(out);
    //prompts the user to input the index
   System.out.print("Enter the index ");
   index = scan.nextInt();
    //checking the whether the index is within the range
    if (index >= 0 && index <= 9) {
   newArray1 = delete(num,index); //calling the method that deletes the one member of the array
    System.out.println("Index " + index + " element is removed");
    out1="The output array is ";
    out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
    System.out.println(out1);
    }
    else {
      //if it is out of the range, print out the original array without changing
      newArray1 = num;
      System.out.println("The index is not valid.");
      out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
    }
    //////
   
    //prompts the user to enter a target value
    System.out.print("Enter the target value ");
   target = scan.nextInt();
    //checking whether the target exists in the array
    for (i = 0; i < num.length; i++) {
      if (num[i] == target) { //if one element equals to the target, break the for loop
        newArray2 = remove(num,target);
        System.out.println("Element " + target + " is found");
        String out2="The output array is ";
        out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
        System.out.println(out2);
        break;
      }
      if (num[i] != target) {
        if (i == num.length - 1) {
        System.out.println("Element " + target + " is not found."); 
        String out2="The output array is ";
        out2+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
        System.out.println(out2);
        }
        continue;
      }
    }
    /////////
   System.out.print("Go again? Enter 'y', anything else to quit-");
   answer=scan.next(); //user inputs the answer
 }
    while(answer.equals("y"));
  }
  //adding the punctuations to the print out of the array members
  public static String listArray(int num[]){ 
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }

  
}