public class Arithmetic {
 
  public static void main(String[] args) {
    int numPants = 3;//Number of pairs of pants
    double pantsPrice = 34.98;//Cost per pair of pants
    
    int numShirts = 2;//Number of sweatshirts
    double shirtPrice = 24.99;//Cost per shirt
    
    int numBelts = 1;//Number of belts
    double beltCost = 33.99;//cost per box of envelopes
    
    double paSalesTax = 0.06;//the tax rate
    
    //Total cost of pants
    double totalCostOfPants= numPants*pantsPrice;
   
    //Total cost of sweatshirts
    double totalCostOfShirts= numShirts* shirtPrice;
    
     //Total cost of belts
    double totalCostOfBelts= numBelts*beltCost; 
    

    //sales tax charged on pants
    double totalTaxOnPants=  totalCostOfPants*paSalesTax;
      
    //sales tax charged on sweatshirts
    double totalTaxOnShirts= totalCostOfShirts*paSalesTax;
    
    //sales tax charged on belts
    double totalTaxOnBelts=  totalCostOfBelts* paSalesTax;
    
    //Total cost of purchases (before tax)
    double totalCostBeforeTax=  totalCostOfPants+ totalCostOfShirts+  totalCostOfBelts;
    
    //Total sales tax
    double totalTax= totalTaxOnPants+  totalTaxOnShirts+ totalTaxOnBelts;
    
    //Total paid for this transaction, including sales tax. 
    double totalCost=  totalCostBeforeTax+  totalTax;
    
    System.out.println("The total cost of pants is "+ totalCostOfPants+"." );
    System.out.println("The total cost of sweatshirts is "+totalCostOfShirts+"." );
    System.out.println("The total cost of belts is "+totalCostOfBelts+".");
    System.out.println("The sales tax charged on pants is "+String.format("%.2f",totalTaxOnPants));
    System.out.println("The sales tax charged on sweatshirts is "+ String.format("%.2f",totalTaxOnShirts));
    System.out.println("The sales tax charged on belts is "+ String.format("%.2f",totalTaxOnBelts));
    System.out.println("The total cost of purchases(before tax) is "+ String.format("%.2f",totalCostBeforeTax)+"." );
    System.out.println("The total sales tax is "+ String.format("%.2f",totalTax)+"." );
    System.out.println("The total paid for this transaction, including sales tax is "+String.format("%.2f",totalCost)+"." );
  }
 
  
  
  
}

