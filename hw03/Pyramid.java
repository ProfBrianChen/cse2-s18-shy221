//02.12.2018 CSE002 Yushen Yi This programme aims to prompt the user for the dimensions of a pyramid and return the volume inside the pyramid.
import java.util.Scanner;
public class Pyramid{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);//input new datas from user
    System.out.print("The square side of the pyramid is: ");//input square side of the pyramid 
    double length = myScanner.nextDouble();//input by users
    System.out.print("The height of the pyramid is: ");//input height of pyramid
    double height = myScanner.nextDouble();//input by users
    double volume= length*length*height/3;//calculate the the volume inside the pyramid
    System.out.println("The volume inside the pyramid is "+ volume+ ".");//print the volume inside the pyramid
  }
}