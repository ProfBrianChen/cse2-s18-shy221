//Shenyi Yu 2/2/2018 CSE002 The file perform a software of a bicycle cyclometer that records two kinds of data including 1.The time elaspes in seconds 2. the number of rotations of the front wheel during that time of a bicycle.
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      // our input data.
    int secsTrip1=480;  //The first trip costs 480s
    int secsTrip2=3220;  //The second trip costs 480s 3220s
		int countsTrip1=1561;  //The number of rotation for the first trip is 1561
		int countsTrip2=9037; //The number of rotation for the second trip is 9037
     // our intermediate variables and output data.
    double wheelDiameter=27.0,  //The diameter for the wheel is 27.0 
  	PI=3.14159, //The value of pi used in here is 3.14159
  	feetPerMile=5280,  //The conversion from mile to feet is 5280ft/mile
  	inchesPerFoot=12,   //The conversion from foot to inches is 12in/ft
  	secondsPerMinute=60;  //There are 60s in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;//declare the variable distance of Trip1: distanceTrip1; distance of Trip2 :distanceTrip2; total distance: totalDistance
      
   System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
	 System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");

      //run the calculations; store the values. Document your
		//calculation here. What are you calculating?
		//The calculation done below is to calculate the distance traveled for Trip1 and Trip2 by mutiply the parameters of the wheels by counts
		//The distance will then convert into miles
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//does the calculation in the same way of calculating distanceTrip1 to distanceTrip2
	totalDistance=distanceTrip1+distanceTrip2;//determine the totalDistance by the sum of distanceTrip1 and distanceTrip2
//Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class