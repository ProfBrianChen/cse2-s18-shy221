////CSE002 Yushen Yi This programme aims to print "Hello,World" to the terminal window.
public class WelcomeClass {
 
  
  public static void main(String[] args) {
    //Print "Hello,World" to the terminal window.
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-S--H--Y--2--2--1->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("v  v  v  v  v  v");
  }
 
  
  
  
}